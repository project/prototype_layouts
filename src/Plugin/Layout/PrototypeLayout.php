<?php

namespace Drupal\prototype_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\style_options\Plugin\Layout\StyleOptionLayoutPlugin;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class definition for PrototypeLayout layout plugin.
 */
class PrototypeLayout extends StyleOptionLayoutPlugin implements PluginFormInterface {

  /**
   * {@inheritdoc}
   *
   * Remove administrative label.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (method_exists(get_parent_class($this), 'buildConfigurationForm')) {
      $form = parent::buildConfigurationForm($form, $form_state);
      unset($form['label']);
    }
    return $form;
  }

}
